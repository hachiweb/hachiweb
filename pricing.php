<?php
include("header.php");
?>

    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-8 ftco-animate text-center text-center mt-5">
          	<p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Pricing</span></p>
            <h1 class="mb-3 bread">Pricing</h1>
          </div>
        </div>
      </div>
    </div>
  
    <section class="ftco-section">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-4 text-center ftco-animate">
    				<div class="steps">
    					<div class="icon active mb-4 d-flex justify-content-center align-items-center">
    						<span>1</span>
    					</div>
    					<h3>Choose Your Plan</h3>
    					<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
    				</div>
    			</div>
    			<div class="col-md-4 text-center ftco-animate">
    				<div class="steps">
    					<div class="icon mb-4 d-flex justify-content-center align-items-center">
    						<span>2</span>
    					</div>
    					<h3>Create Your Account</h3>
    					<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
    				</div>
    			</div>
    			<div class="col-md-4 text-center ftco-animate">
    				<div class="steps">
    					<div class="icon mb-4 d-flex justify-content-center align-items-center">
    						<span>3</span>
    					</div>
    					<h3>Launch</h3>
    					<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="ftco-section bg-primary">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-4">Our Best Pricing</h2>
            <p>We understand your requirement and provide quality works.</p>
          </div>
        </div>
    		<div class="row no-gutters d-flex">
	        <div class="col-lg-4 col-md-6 ftco-animate d-flex">
	          <div class="block-7 bg-light d-flex align-self-stretch">
	            <div class="text-center">
		            <h2 class="heading">Website</h2>
		            <span class="price"><small class="per">Starting from</small><p class="number">$250</p>
		            <span class="excerpt d-block">100% Forever</span>
		            <h3 class="heading-2 mb-3">Enjoy All The Features</h3>
		            
		            <ul class="pricing-text mb-4">
		              <li><strong>Responsiveness & Quality</strong></li>
		              <li><strong>7-10 Screen</strong></li>
		              <li><strong>Mobile Compatible</strong></li>
		              <li><strong>Platform:-Word Press, Zoho, Wix & Much More</strong></li>
		            </ul>
		            <a href="#" class="btn btn-success d-block px-3 py-3 mb-4">Choose Plan</a>
	            </div>
	          </div>
	        </div>
	        <div class="col-lg-4 col-md-6 ftco-animate d-flex">
	          <div class="block-7 d-flex align-self-stretch">
	            <div class="text-center">
		            <h2 class="heading">Android Application</h2>
		            <span class="price"><small class="per">Starting from</small><p class="number">$250</p>
		            <span class="excerpt d-block">All features are included</span>
		            <h3 class="heading-2 mb-3">Enjoy All The Features</h3>
		            
		            <ul class="pricing-text mb-">
		              <li><strong>Responsiveness & Quality</strong></li>
		              <li><strong>8-10 Screen</strong></li>
		              <li><strong>Included Source Code</strong></li>
		              <li><strong>Admin System & Much More</strong></li>
		            </ul>
		            <a href="#" class="btn btn-success d-block px-3 py-3 mt-5">Choose Plan</a>
	            </div>
	          </div>
	        </div>
	        <div class="col-lg-4 col-md-6 ftco-animate d-flex">
          <div class="block-7 bg-light d-flex align-self-stretch">
            <div class="text-center">
		            <h2 class="heading">Logo Design</h2>
		            <span class="price"><small class="per">Starting from</small><p class="number">$50</p>
		            <span class="excerpt d-block">All features are included</span>
		            <h3 class="heading-2 mb-3">Enjoy All The Features</h3>
		            
		            <ul class="pricing-text mb-">
		              <li><strong>Unlimited</strong></li>
		              <li><strong>More Than 10,000+ Design To Choose</strong></li>
		              <li><strong>Custom Design & Much More</strong></li>
		              <li><strong></strong></li>
		            </ul>
		            <a href="#" class="btn btn-success d-block px-3 py-3" style="margin-top:80px;">Choose Plan</a>
	            </div>
	          </div>
	        </div>
	        <!-- <div class="col-lg-3 col-md-6 ftco-animate d-flex">
	          <div class="block-7 active d-flex align-self-stretch">
	            <div class="text-center">
		            <h2 class="heading">Pro</h2>
		            <span class="price"><sup>$</sup> <span class="number">99<small class="per">/mo</small></span></span>
		            <span class="excerpt d-block">All features are included</span>
		            <h3 class="heading-2 mb-3">Enjoy All The Features</h3>
		            
		            <ul class="pricing-text mb-4">
		              <li><strong>450 GB</strong> Bandwidth</li>
		              <li><strong>400 GB</strong> Storage</li>
		              <li><strong>$20.00 / GB</strong> Overages</li>
		              <li>All features</li>
		            </ul>
			          <a href="#" class="btn btn-secondary d-block px-3 py-3 mb-4">Choose Plan</a>
	            </div>
	          </div>
	        </div> -->
	      </div>
    	</div>
    </section>

    <section class="ftco-section services-section bg-light ftco-no-pb pb-5">
      <div class="container">
      	<div class="row justify-content-center mb-5 pb-5">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">Why You Should Choose Us</h2>
            <p>At Hachiweb, we develop our trade by maximizing your commerce. Each web plan company points to form
              cash so do we. But our primary center is to assist your trade to create more cash. Our objective is
              to construct believe and long term relationship with our clients. Numerous web plan company in
              Dehradun, Uttrakhand, India fair need a speedy offer without considering client's Return on
              Speculation (ROI).</p>
          </div>
        </div>
        <div class="row">
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-cloud"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Super Fast Service</h3>
                        <p class="mb-0">Hachiweb is one of the leading Web Advancement Company which offers reliable Web
                            Improvement Administrations around the world to create the foremost momentous comes about &
                            fortify your commerce.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-server"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Daily Backups</h3>
                        <p class="mb-0">What would happen in the event that you woke up tomorrow and your web have had
                            incidentally erased your site? It happened to me. And, you speculated it: I didn't have a
                            reinforcement. That won't ever happen to me once more and I'm here to assist you get
                            arranged as well.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-customer-service"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Technical Services</h3>
                        <p>We Plan & Create, Appealing, Transformation Neighborly, Full Versatile Responsive, Quick
                            Stacking, Security-Optimized and SEO Inviting Websites that will offer assistance to make a
                            important to begin with impression.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-life-insurance"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Secure and Reliable</h3>
                        <p>Site Plan and Advancement experts plan websites with security in Intellect perfect way". The
                            most perfect way to create a location secure and dependable is the application of a firewall
                            to secure your location. The firewall anticipates interruption by blocking the passage of
                            pernicious websites.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-cloud-computing"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">DNS Control</h3>
                        <p>Domain names are shaped by the rules and strategies of the Domain Name System (DNS). Domain
                            enrollment is the method by which a company or individual can secure website space, Once
                            this handle is total the Domain enrollment the space gets to be yours for the period of the
                            contract which is ordinarily one year. And afterward needs to be recharged.</p>
                    </div>
                </div>
            </div>
        </div>
      </div>
	</section>
	
	<section class="ftco-section ftco-counter img" id="section-counter">
    	<div class="container">
    		<div class="row justify-content-center mb-5">
          <div class="col-md-10 text-center heading-section ftco-animate">
            <h2 class="mb-4">More than 12,000 websites trusted</h2>
          </div>
        </div>
    		<div class="row justify-content-center">
          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="12000">0</strong>
                <span>CMS Installation</span>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="100">0</strong>
                <span>Awards Won</span>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="10000">0</strong>
                <span>Registered Domains</span>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="9000">0</strong>
                <span>Satisfied Customers</span>
              </div>
            </div>
          </div>
        </div>
    	</div>
    </section>

    

    
    <?php
    include("footer.php");
    ?>