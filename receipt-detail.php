<?php
if (session_status() == 0) {

}
session_start();
// echo session_id();
// echo ini_get('session.cookie_domain');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("header.php");
include("connection.php");



$id = $_POST['receipt_number'];
$id = mysqli_real_escape_string($conn,$id);
$query = "SELECT * FROM `contact_details` WHERE `id`='" . $id . "'";
$result = mysqli_query($conn,$query);
while($row = mysqli_fetch_array($result)) {
    $contact['contact_first_name'] = $row['contact_first_name'];
    $contact_first_name = $row['contact_first_name'];

    $contact['contact_last_name'] = $row['contact_last_name'];
    $contact_last_name = $row['contact_last_name'];

    $contact['contact_address_line1'] = $row['contact_address_line1'];
    $contact_address_line1 = $row['contact_address_line1'];

    $contact['contact_address_line2'] = $row['contact_address_line2'];
    $contact_address_line2 = $row['contact_address_line2'];

    $contact['contact_zip_code'] = $row['contact_zip_code'];
    $contact_zip_code = $row['contact_zip_code'];

    $contact['contact_phone_number'] = $row['contact_phone_number'];
    $contact_phone_number = $row['contact_phone_number'];

    $contact['contact_email_id'] = $row['contact_email_id'];
    $contact_email_id = $row['contact_email_id'];

    $contact['contact_country_name'] = $row['contact_country_name'];
    $contact_country_name = $row['contact_country_name'];

    $contact['contact_state_name'] = $row['contact_state_name'];
    $contact_state_name = $row['contact_state_name'];

    $contact['contact_city_name'] = $row['contact_city_name'];
    $contact_city_name = $row['contact_city_name'];

    $contact['project_services_name'] = $row['project_services_name'];
    $project_services_name = $row['project_services_name'];

    $contact['project_file_upload_name'] = $row['project_file_upload_name'];
    $project_file_upload_name = $row['project_file_upload_name'];

    $contact['project_description'] = $row['project_description'];
    $project_description = $row['project_description'];

}

$_SESSION['contact'] = $contact;

?>

<div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');"
    data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-8 ftco-animate text-center text-center mt-5">
                <p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.php">Home <i
                                class="ion-ios-arrow-forward"></i></a></span> <span>Receipt Details</span></p>
                <h1 class="mb-3 bread">Receipt Details</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section contact-section bg-primary">
    <div class="container">
        <div class="row block-9 justify-content-center mb-5">
            <div class="col-md-12 mb-md-5 questions">
                <h2 class="text-center text-uppercase shadow p-2">Showing the Result for the Receipt Number: <span>123456789</span></h2>
                <div class="bg-light p-5 ">
                    <div class="row contact_sec shadow p-4">
                        <div class="col-sm-12 mb-5">
                            <div class="contact_detail text-center">
                                <span class="text-center shadow">Contact Details</span>
                            </div>
                        </div><!-- col-12-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h5>Full Name: <span><?php echo $contact_first_name. ' ' .$contact_last_name; ?></span></h5>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h5>Full Address: <span><?php echo $contact_address_line1. ' ' .$contact_address_line2; ?></span></h5>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h5>Zip Code: <span><?php echo $contact_zip_code; ?></span></h5>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h5>Phone Number: <span><?php echo $contact_phone_number; ?></span></h5>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h5>Email Id: <span><?php echo $contact_email_id; ?></span></h5>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h5>Country: <span><?php echo $contact_country_name; ?></span></span></h5>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h5>State: <span><?php echo $contact_state_name; ?></span></span></h5>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <h5>City: <span><?php echo $contact_city_name; ?></span></span></h5>
                        </div><!-- col-6-->
                    </div> <!-- end contect details -->

                    <div class="row contact_sec shadow p-4">
                        <div class="col-12 my-5">
                            <div class="contact_detail text-center">
                                <span class="text-center shadow">Project Description</span>
                            </div>
                        </div><!-- col-12-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <h5>Services: <span><?php echo $project_services_name; ?></span></span></h5>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <h5>Description: <span><?php echo $project_description; ?></span></span></h5>
                            </div>
                        </div> <!-- col-12-->
                    </div> <!-- end project description -->

                    <div class="row shadow p-4">
                        <!-- start billing details -->
                        <div class="col-12 my-5">
                            <div class="contact_detail text-center">
                                <span class="text-center shadow">Billing Details</span>
                            </div>
                        </div><!-- col-12-->
                        <div class="col-md-12">
                            <div class="row mt-3 contact_sec">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h5>Full Name: <span>XYZ</span></h5>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h5>Full Address: <span>XYZ</span></h5>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h5>Zip Code: <span>98765</span></h5>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h5>Phone Number: <span>9876543210</span></h5>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h5>Email Id: <span>xyz@gmail.com</span></h5>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h5>Country: <span>India</span></h5>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h5>State: <span>Uttrakhand</span></h5>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <h5>City: <span>Dehradun</span></h5>
                                </div><!-- col-6-->
                            </div>
                        </div><!-- col-12-->
                    </div>
                    <!--end billing Details -->

                    <div class="row shadow p-4">
                        <!--start Payment Details -->
                        <div class="col-12 my-5">
                            <div class="contact_detail text-center">
                                <span class="text-center shadow">Payment Details</span>
                            </div>
                        </div><!-- col-12-->
                        <div class="col-md-12">
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <div>
                                        <h5>Billing Information</h5>
                                        <h6>Enter Your Payment Details Bellow</h6>
                                    </div>
                                </div><!-- col-12-->
                            </div><!-- row-->
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12 my-auto">
                                    <div class="payment-method">
                                        <h5>Payment Method <span class="text-danger">*</span></h5>
                                    </div>
                                </div><!-- col-4-->
                                <div class="col-lg-5 col-md-8 col-sm-12">
                                    <!-- <div class="payment_method_other">
                                        <img src="images/visa.png" alt="image not found">
                                    </div> -->
                                    <div class="payment_method_other cc-selector-2">
                                        <input id="paypal" type="radio" class="payment_gateway form-control"
                                            name="for_other" value="paypal" />
                                        <label class="drinkcard-cc paypal" for="paypal"></label>
                                        <!-- <input id="visa" type="radio" class="payment_gateway" name="for_other" value="visa" />
                                        <label class="drinkcard-cc visa"for="visa"></label> -->
                                    </div>
                                    <div class="payment_method_india cc-selector-2">
                                        <input id="visa2" type="radio" class="payment_gateway form-control"
                                            name="for_india" value="payu" />
                                        <label class="drinkcard-cc payu" for="visa2"></label>
                                        <input id="ccavenue" type="radio" class="payment_gateway" name="for_india"
                                            value="ccavenue" />
                                        <label class="drinkcard-cc ccavenue" for="ccavenue"></label>
                                    </div>
                                </div><!-- col-5-->
                                <div class="col-lg-4 col-md-12 col-sm-12 my-auto">
                                    <div class="payble_amount">
                                        <h5 class="shadow">Amount Paid: <span>$250</span></h5>
                                    </div>
                                </div><!-- col-6-->
                            </div>
                        </div><!-- col-12-->
                    </div>
                    <!--row-->
                    <div class="row submit-button">
                        <div class="col-sm-12 text-center">
                          <button type="button" class="btn btn_submit shadow btn-primary printMe py-2"> PRINT <i class="fas fa-print"></i></button>
                        </div>
                    </div>
                </div>
            </div><!-- col-12-->
        </div>
    </div>
</section>
<script>
$('.printMe').click(function(evt) {
    evt.preventDefault();
    $('body').append('<iframe src="print.php?param=value" id="printIFrame" name="printIFrame"></iframe>');
    $('#printIFrame').bind('load', function() { 
            window.frames['printIFrame'].focus(); 
            window.frames['printIFrame'].print(); 
        }
    );
});
</script>

<?php
    include("footer.php");
?>