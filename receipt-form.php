<?php
include("header.php");
?>

    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-8 ftco-animate text-center text-center mt-5">
          	<p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Receipt Number</span></p>
            <h1 class="mb-3 bread">Receipt Number</h1>
          </div>
        </div>
      </div>
    </div>
 <!-- Services -->
 <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <div class="img-set text-center">
            <img src="images/hachiweb.png" alt="">
            </div>
            <!-- <h5 class="card-title text-center mt-3">Download Signed Admission Form</h5> -->
            <form action="receipt-detail.php" class="form-signin my-5" method="POST">
              <div class="form-label-group text-center">
                <label for="inputEmail my-2">Enter Your Receipt Number</label>
                <input type="text" id="inputEmail" class="form-control" name="receipt_number" placeholder="Receipt Number" required autofocus>
              </div>
              <div class="row">
             <!-- <div class="col-12">
             <div class="g-recaptcha mt-3" id="rcaptcha" data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
              <span id="captcha" style="color:red"></span>
             </div> -->
              </div>
              <input type="submit" class="btn btn-lg btn-primary btn-block text-uppercase my-5" value="SUBMIT"/>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- end Services -->
  <!-- <script>
  
  $(document).on('submit','.form-signin',function(e){
    var v = grecaptcha.getResponse();
    if(v.length == 0)
    {
        document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
        e.preventDefault();
        return false;
    }
    else
    {
        document.getElementById('captcha').innerHTML="Captcha completed";
        return true; 
    }
  });
  </script> -->
<?php
include("footer.php");
?>