<?php
include("connection.php");
$query = "SELECT * FROM `certification-details`";
$result = mysqli_query($conn,$query);
while($row = mysqli_fetch_array($result)) {
    $reference_number_db = $row['reference_number'];
    $first_name = $row['first_name'];
    $last_name = $row['last_name'];
    $date_of_birth_db = $row['date_of_birth'];
    $job_role = $row['job_role'];
    $certification_type = $row['certification_type'];
    $appointed_on = $row['appointed_on'];
    $employed_till = $row['employed_till'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <style>
    </style>
</head>
<style>
body {
    /* margin-bottom: 50px; */
    background-color:#eaf8fb;
}
span{
    color:black;
    font-size:16px;
    font-weight:600;
    margin-left:6px;
}

</style>

<body>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-10 col-md-10 col-lg-8 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <div class="img-set text-center mb-4">
                            <img src="images/hachiweb.png" alt="">
                        </div>
                        <div class="certify_detail text-center">
                                <span class="text-center">Certificate Details Found for Reference Number: - <?php echo $reference_number_db; ?></span>
                            </div>
                        <div class="row my-5">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h6>First Name:<span><?php echo $first_name; ?></span></h6>
                                </div>
                            </div><!-- col-6-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h6>Last Name:<span><?php echo $last_name; ?></span></h6>
                                </div>
                            </div><!-- col-6-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h6>Date of Birth: <span><?php echo $date_of_birth_db; ?></span></h6>
                                </div>
                            </div><!-- col-6-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h6>Job Role: <span><?php echo $job_role; ?></span></h6>
                                </div>
                            </div><!-- col-6-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h6>Certification Type: <span><?php echo $certification_type; ?></span></h6>
                                </div>
                            </div><!-- col-6-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h6>Appointed On: <span><?php echo $appointed_on; ?></span></h6>
                                </div>
                            </div><!-- col-6-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h6>Employed Till: <span><?php echo $employed_till; ?></span></h6>
                                </div>
                            </div><!-- col-6-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>