<?php
include("header.php");
?>

    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-8 ftco-animate text-center text-center mt-5">
          	<p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Services</span></p>
            <h1 class="mb-3 bread">Services</h1>
          </div>
        </div>
      </div>
    </div>
 <!-- Services -->
 <section class="page-section" id="services">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Our Services</h2>
          <h3 class="section-subheading text-muted">To attain a competitive advantage in a fiercely competitive market, we offer some exclusive services<br> as well as smashing tactics.Peep at the given list of services we provide to our customers:</h3>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-wordpress fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">WordPress</h4>
          <p class="text-muted text-justify">WordPress is the world’s most popular tool for creating websites. WordPress is capable of creating any style of website, from a simple blog to a full-featured business website. You can even use WordPress to create an online store (using the popular WooCommerce plugin).</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-react fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">ReactJS</h4>
          <p class="text-muted text-justify">ReactJS is an open-source JavaScript library which is used for building user interfaces specifically for single page applications. It’s used for handling view layer for web and mobile apps. React also allows us to create reusable UI components.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-node-js fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">NodeJs</h4>
          <p class="text-muted text-justify">Node.js is an open-source server side runtime environment built on Chrome's V8 JavaScript engine. It provides an event driven, non-blocking (asynchronous) I/O and cross-platform runtime environment for building highly scalable server-side application using JavaScript.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-mobile fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Ionic</h4>
          <p class="text-muted text-justify">Ionic Framework is focused on the frontend user experience, or UI interaction of an app (controls, interactions, gestures, animations). It’s easy to learn, and integrates nicely with other libraries or frameworks, such as Angular, or can be used standalone without a frontend framework using a simple script include.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Shopify</h4>
          <p class="text-muted text-justify">Shopify is one of the biggest and most popular E-commerce platforms, with which anyone can create an e-commerce website or online store, and start selling their products online. It is a framework purely designed to create an e-commerce website and is the easiest one to go with.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-laravel fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Laravel</h4>
          <p class="text-muted text-justify">Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-php fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">PHP</h4>
          <p class="text-muted text-justify">PHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting language that is especially suited for web development and can be embedded into HTML.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-wix fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Wix</h4>
          <p class="text-muted text-justify">This powerful technology makes it easy for everyone to get online with a stunning, professional and functional web presence. Whether it’s your first time creating or you’re a long time expert, you’ll find the features and solutions you need to build a professional website with true creative freedom.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-lock fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Zoho</h4>
          <p class="text-muted text-justify">Customer relationship management, otherwise known as a CRM, is one of the most helpful tools that any business could use. So, what is CRM software and what is Zoho CRM?  Some people refer to a CRM system as a rolodex on steroids. It’s a one-stop shop where you can keep all of you client’s contact information, organize your business deals, track marketing campaigns, and so much more.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-globe fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">API</h4>
          <p class="text-muted text-justify">An application program interface (API) is a set of routines, protocols, and tools for building software applications. Basically, an API specifies how software components should interact. Additionally, APIs are used when programming graphical user interface (GUI) components. A good API makes it easier to develop a program by providing all the building blocks.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-html5 fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">HTML5</h4>
          <p class="text-muted text-justify">HTML5 is a programming language whose acronym stands for Hyper Text Markup Language. It is a system that allows the modification of the appearance of web pages, as well as making adjustments to their appearance. It also used to structure and present content for the web.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-css3-alt fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">CSS3</h4>
          <p class="text-muted text-justify">Cascading Style Sheets (CSS) is a language that is used to illustrate the look, style, and format of a document written in any markup language. In simple words, it is used to style and organize the layout of Web pages. CSS3 is the latest version of an earlier CSS version, CSS2.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-bootstrap fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Bootstrap4</h4>
          <p class="text-muted text-justify">Bootstrap 4 is the newest version of Bootstrap, which is the most popular HTML, CSS, and JavaScript framework for developing responsive, mobile-first websites. Bootstrap 4 is completely free to download and use!</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fab fa-js-square fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Javascript/Jquery</h4>
          <p class="text-muted text-justify">JavaScript is a dynamic computer programming language. It is lightweight and most commonly used as a part of web pages, whose implementations allow client-side script to interact with the user and make dynamic pages. It is an interpreted programming language with object-oriented capabilities.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- end Services -->

<?php
include("footer.php");
?>