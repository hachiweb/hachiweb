<?php
include("header.php");
?>

    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-8 ftco-animate text-center text-center mt-5">
          	<p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Portfolio</span></p>
            <h1 class="mb-3 bread">Portfolio</h1>
          </div>
        </div>
      </div>
    </div>
  <!-- Portfolio Grid -->
  <section class="bg-light page-section" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Portfolio</h2>
          <h3 class="section-subheading text-muted">Project Type- Web.</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/bellshipitnow.PNG" alt="Bellshipitnow redesign project by hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>Bellshipitnow</h4>
            <p class="text-muted"><a href="https://www.bellshipitnow.com/">https://www.bellshipitnow.com/</a></p>
          </div>
        </div><!--project-->
        
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/garlandprosolution.PNG" alt="Garland Pro Solution development project by hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>Garland Pro Solution</h4>
            <p class="text-muted"><a href="http://garlandprosolutions.net/">http://garlandprosolutions.net/</a></p>
          </div>
        </div><!--project-->

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/mco.PNG" alt="myclickonline website development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>MyClickOnline</h4>
            <p class="text-muted"><a href="http://mco.hachistaging.com/">http://mco.hachistaging.com/</a></p>
          </div>
        </div><!--project-->

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/shiva-tubewells.PNG" alt="Shiva Tubewells website development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>Shiva Tubewells</h4>
            <p class="text-muted"><a href="https://www.shivatubewells.com/">https://www.shivatubewells.com/</a></p>
          </div>
        </div><!--project-->

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/thesauce.PNG" alt="The Sauce website development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>The Sauce</h4>
            <p class="text-muted"><a href="http://www.thesauce.nz/">http://www.thesauce.nz/</a></p>
          </div>
        </div><!--project-->

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/truvim.PNG" alt="truvim website development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>Truvim</h4>
            <p class="text-muted"><a href="https://www.truvim.com/">https://www.truvim.com/</a></p>
          </div>
        </div><!--project-->

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal7">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/bacmetall.PNG" alt="bacmetall website development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>Bacmetall</h4>
            <p class="text-muted"><a href="https://bacmetall.lt/en/home/">https://bacmetall.lt/en/home/</a></p>
          </div>
        </div><!--project-->

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal8">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/bacmetall-app.PNG" alt="Bacmetall Android Application development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>Bacmetall Android Application</h4>
            <p class="text-muted"><a href="https://play.google.com/store/apps/details?id=bacmetall.release&hl=gsw">https://play.google.com/store/apps/details?id=bacmetall.release&hl=gsw</a></p>
          </div>
        </div><!--project-->

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal9">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/yabayya.jpg" alt="Bacmetall Android Application development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>Yabayya</h4>
            <p class="text-muted"><a href="https://www.yabayaa.com/">https://www.yabayaa.com/</a></p>
          </div>
        </div> <!--project-->

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal10">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/msf.jpg" alt="MySafetyFirst Android Application development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>MySafetyFirst</h4>
            <p class="text-muted"><a href="https://mysafetyfirst.net/">https://mysafetyfirst.net/</a></p>
          </div>
        </div> <!--project-->

        
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal11">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/portfolio/olf.jpg" alt="MySafetyFirst Android Application development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>One L1fe Fitness</h4>
            <p class="text-muted"><a href="http://olf.hachistaging.com/">http://olf.hachistaging.com/</a></p>
          </div>
        </div> <!--project-->


        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal12">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="images/image_5_stw.jpg" alt="Shiva Tubewells Android Application development hachiweb">
          </a>
          <div class="portfolio-caption">
            <h4>Borewell Dehradun</h4>
            <p class="text-muted"><a href="https://play.google.com/store/apps/details?id=com.shivatubewells.borewelldehradun&hl=en_IN/"></a></p>
          </div>
        </div> <!--project-->

      </div>
    </div>
  </section>

  <!-- Portfolio Modals -->

  <!-- Modal 1 -->
  <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Bellshipitnow</h2>
                <h4 class="item-intro text-dark">Wordwide Shipping Company</h4>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/bellshipitnow.PNG" alt="bellshipitnow Wordwide Shipping Company, development by hachiweb">
                <p>We are your own shipper, and coordinations the executives organization that control the effective delivery, and capacity of your merchandise. We likewise administration, and relate data between the purpose of root and the purpose of utilization so as to address your issues.</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Threads</li>
                  <li>Category: Illustration</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 2 -->
  <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Garland Pro Solution</h2>
                <h4 class="item-intro text-dark">Project Type- Web.</h4>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/garlandprosolution.PNG" alt="Garland Pro Solution development hachiweb dehradun">
                <p>Garland Pro Solutions is the primary car just stage to twofold check your CRM and fix the well established issue of terrible information! Our organization values ensuring your numbers are right and you can concentrate on offering vehicles to the right individuals!</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Explore</li>
                  <li>Category: Graphic Design</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 3 -->
  <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">MyClickOnline</h2>
                <p class="item-intro text-muted">Project Type- Web.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/mco.PNG" alt="myclickonline development by hachiweb dehradun">
                <p>Generally known as India's no. 1 online classifieds stage, MyClickOnline is about you. Our point is to engage each individual in the nation to autonomously interface with purchasers and dealers on the web. We care about you — and the exchanges that carry you closer to your fantasies. Need to purchase your first vehicle? We're here for you. Need to offer business property to purchase your fantasy home? We're here for you. Whatever employment you have, we guarantee to complete it.</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Finish</li>
                  <li>Category: Identity</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 4 -->
  <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Shiva Tubewells</h2>
                <p class="item-intro text-muted">Project Type- Web.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/shiva-tubewells.PNG" alt="shiva tubewells development by hachiweb uttrakhand">
                <p>We are Dehradun based govt. regd. driving water boring firm. We are declared authoritatively in the year 2004 yet we've been working much earlier.We hold tremendous boring experience. We have skill in 5, 7, and 8" Odex penetrating.</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Lines</li>
                  <li>Category: Branding</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 5 -->
  <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">The Sauce</h2>
                <p class="item-intro text-muted">Project Type- Web.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/thesauce.PNG" alt="the sauce development by hachiweb dehradun">
                <p>We're individuals – simply like you. We're driven, energetic, sympathetic and well disposed. We care about individuals – this is the reason we do what we do. We set aside the effort to tune in and comprehend your needs or prerequisites.</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Southwest</li>
                  <li>Category: Website Design</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 6 -->
  <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Truvim</h2>
                <p class="item-intro text-muted">Project Type- Web.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/truvim.PNG" alt="truvim development hachiweb uttrakhand">
                <p>Essentially preparing and creating numbers isn't the finished result our customers need from us. Our customers need imagination, energy and development. We are here to help the two choices producers and partners in the dynamic business network. We endeavor to give the best help productively and viably and are devoted to the three fundamental standards of demonstrable skill, responsiveness and quality.</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Window</li>
                  <li>Category: Photography</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 7 -->
  <div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Bacmetall</h2>
                <p class="item-intro text-muted">Project Type- Web.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/bacmetall.PNG" alt="bacmetall development by hachiweb dehradun uttrakhand">
                <p>Bacmetall was shaped in 2007 through the impetus business blast. 10 years of involvement with this market enables us to exhort our customers and give them the best working conditions. It additionally adds to the decrease of contamination by the auspicious transfer of car impetuses, and is successful in returning them to the market subsequent to reusing.</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Window</li>
                  <li>Category: Photography</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 8 -->
  <div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Bacmetall Android Application</h2>
                <p class="item-intro text-muted">Project Type- Mobile.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/bacmetall-app.PNG" alt="Bacmetall Android Application development by hachiweb company uttrakhand">
                <p>BacMetal application made for astounding client experience and solace. In the event that you have unused catalyst(s), essentially join, discover your impetus and add it to your truck, yet for this situation we are paying you for your stuff as per current market costs. Simultaneously you are adding to nature protection and getting benefits for your choice.</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Window</li>
                  <li>Category: Photography</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
  <!-- Modal 9 -->
  <div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Yabayya</h2>
                <p class="item-intro text-muted">Project Type- Web.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/yabayya.jpg" alt="Yabayya development by hachiweb company uttrakhand">
                <p>Yabayaa Merchandising Ltd is a Business to Business supplier of plain and customized clothing and accessories offering a wealth of ancillary services including screen printing, embroidery, direct to garment printing and garment finishing. </p>
                <ul class="list-inline">
                  <li>Date: January 2020</li>
                  <li>Client: Team Digi Vision</li>
                  <li>Category: E-commerce</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 10 -->
  <div class="portfolio-modal modal fade" id="portfolioModal10" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">MySafetyFirst</h2>
                <p class="item-intro text-muted">Project Type- Web.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/msf.jpg" alt="MySafetyFirst development by hachiweb company uttrakhand">
                <p>My Safety First is a family owned company providing safety consulting and training services in construction and industrial sectors. We take pride in our expertise and no-nonsense approach to safety.</p>
                <ul class="list-inline">
                  <li>Date: January 2020</li>
                  <li>Client: Sanja Santrac</li>
                  <li>Category: Training</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 11 -->
  <div class="portfolio-modal modal fade" id="portfolioModal11" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">One L1fe Fitness</h2>
                <p class="item-intro text-muted">Project Type- Web.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/olf.jpg" alt="One L1fe Fitness development by hachiweb company uttrakhand">
                <p>It was easy to say, there was no other fitness space like this. Exercise blended perfectly with pleasure. You could be anyone you wanted to be. Be any shape you happened to be.. It was simply just, serious fun.</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Window</li>
                  <li>Category: Photography</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

 <!-- Modal 12 -->
  <div class="portfolio-modal modal fade" id="portfolioModal12" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Dehradun Borewell App</h2>
                <p class="item-intro text-muted">Project Type- Mobile.</p>
                <img class="img-fluid d-block mx-auto" src="images/borewell_dehradun.jpg" alt="Borewell Dehradun development by hachiweb company uttrakhand">
                <p>Borewell Dehradun is a one-of-a-kind app supported by Shiva Tubewlls It lets users residing in Himalayan region get instant cost estimate for ODEX and DTH drilling (currently limited to Uttarakhand) in the region. This app also allows users to make request for detailed estimation delivered at submitted email address.</p>
                <ul class="list-inline">
                  <li>Date: Nov 2019</li>
                  <li>Client: Shiva Tubewells</li>
                  <li>Category: Business</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
include("footer.php");
?>