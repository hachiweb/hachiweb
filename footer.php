<footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2 font-weight-normal">Hachiweb</h2>
              <p>Hachiweb (HW) is a boutique digital transformation consultancy headquartered in Dehradun,Uttrakhand Since 2019.</p>
              <ul class="ftco-footer-social list-unstyled mb-0">
                <li class="ftco-animate"><a href="#" alt="twitter like hachiweb company dehradun"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#" alt="facebook like hachiweb company dehradun"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#" alt="instagram like hachiweb company dehradun"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2 font-weight-normal">Here for you</h2>
                <p>Call us 24/7.</p>
                <p>We are there for your support</p>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2 font-weight-normal">Navigational</h2>
              <ul class="list-unstyled">
                <li><a href="index<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" alt="hachiweb developer uttarakhand" class="py-2 d-block">Home</a></li>
                <li><a href="about<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" alt="best web design company uttrakhand" class="py-2 d-block">About</a></li>
                <li><a href="pricing<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" alt="best website pricing in dehradun" class="py-2 d-block">Pricing</a></li>
                <li><a href="portfolio<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" alt="happy client for website design dehradun" class="py-2 d-block">Portfolio</a></li>
                <li><a href="service<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" alt="happy client for website design dehradun" class="py-2 d-block" alt="best service development" class="py-2 d-block">Service</a></li>
                <li><a href="contact<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" alt="hachiweb contact address and number" class="py-2 d-block">Contact</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2 font-weight-normal">Office</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">Hachiweb, NH72, Jhajra, Dehradun, Uttarakhand 248007</span></li>
                  <li><span class="icon icon-phone"></span><span class="text">+91 883 784 2284</span></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@hachiweb.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <p>Copyright &copy; 2019-20 All rights reserved | Powered by <a href="https://www.hachiweb.com" target="_blank" class="text-white">Hachiweb</a></p>
          </div>
        </div>
      </div>
    </footer>
    
  

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
    <!-- <script src="js/jquery.min.js"></script> -->
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="js/google-map.js"></script>
    <script src="js/main.js"></script>
    <script src="js/custom.js"></script>
    <!-- recapcha cdn -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-104912895-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-104912895-1');
    </script>
  </body>
</html>