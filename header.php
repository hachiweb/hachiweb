<?php
  error_reporting(0);
  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
  $link = "https://"; 
  else
  $link = "http://"; 
  $site_url = $link.$_SERVER['HTTP_HOST'];
  $currenturl = $_SERVER['REQUEST_URI'];
  $urlfind = '?'; 
  if (strpos($currenturl, $urlfind) == false) { 
    $currenturl = $_SERVER['REQUEST_URI'];
  } 
  else { 
    $currenturl = substr($currenturl, 0, strpos($currenturl, "?"));
  } 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hachiweb</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Call +91 798 605 0411 - Web Designing, Hachiweb in Dehradun,Uttrakhand is a boutique computerized change consultancy headquartered in Dehradun,Uttrakhand. Since 2015 we have been helping worldwide organizations and built up brands rethink their business by structure effective advanced designing arrangements fueled by the most recent advances technologies."/>    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">

    <link rel="apple-touch-icon" sizes="57x57" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/9818fd8cc550f482328eefa1bda14508.ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/9818fd8cc550f482328eefa1bda14508.ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/9818fd8cc550f482328eefa1bda14508.ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/9818fd8cc550f482328eefa1bda14508.ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/9818fd8cc550f482328eefa1bda14508.ico/favicon-16x16.png">
    <link rel="manifest" href="images/9818fd8cc550f482328eefa1bda14508.ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

   
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Contact form JavaScript -->
    <script src="js/portfoliojs/jqBootstrapValidation.js"></script>
    <script src="js/portfoliojs/contact_me.js"></script>
    <!-- Custom scripts for this template -->
    <script src="js/portfoliojs/agency.min.js"></script>

<!-- testimonial js -->
<!-- <script src="testimonial_js/jquery-3.3.1.min.js"></script> -->
  <script src="testimonial_js/jquery-ui.js"></script>
  <script src="testimonial_js/rangeslider.min.js"></script>
  <script src="testimonial_js/main.js"></script>
  <link rel="stylesheet" href="testimonial_css/custom.css">
  
    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">

    <style>
    .current_page_item {
      background-color: #C00;
      color: #FFF;
      }
    </style>
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
      <a class="navbar-brand" href="index<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">
          <img src="images/hachiweb.png" alt="web development,hachiweb logo" class="w-50">
        </a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto" id="example-one">
	          <li class="nav-item"><a href="index<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" class="nav-link" >Home</a></li>
	          <li class="nav-item"><a href="about<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" class="nav-link about">About</a></li>
            <li class="nav-item"><a class="nav-link" href="pricing<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Pricing</a></li>
            <li class="nav-item dropdown"><a href="" class="nav-link dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Career </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="iht-tnc<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Training</a>
              <a class="dropdown-item" href="https://hrm.hachiweb.com/symfony/web/index<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>/recruitmentApply/jobs.html">Hiring</a>
              <a class="dropdown-item" href="https://hrm.hachiweb.com">HRM</a>
              <a class="dropdown-item" href="https://payroll.hachiweb.com">Payroll</a>
              <a class="dropdown-item" href="https://cbt.hachiweb.com/">CBT</a>
              <a class="dropdown-item" href="verify-certification<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Verify Certification</a>
            </div>
          </li>
            <!-- <li class="nav-item"><a href="iht-tnc<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" class="nav-link">Training </a></li> -->
            <li class="nav-item"><a href="portfolio<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" class="nav-link">Portfolio</a></li>
            <li class="nav-item"><a href="service<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" class="nav-link">Services</a></li>
            <li class="nav-item dropdown"><a href="" class="nav-link dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Payment </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="payment-form<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Make Payment</a>
              <a class="dropdown-item" href="receipt-form<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Check Receipt Number</a>
            </div>
          </li>
	          <li class="nav-item"><a href="contact<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" class="nav-link">Contact</a></li>
	          <!-- <li class="nav-item cta"><a href="contact<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>" class="nav-link"><span>Get started</span></a></li> -->
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->
<script>

$('.navbar-toggler').click(function(){
  $('.collapse').collapse('hide');
});

$(document).on('click',function(){
$('.collapse').collapse('hide');
})

var selector = '.navbar-nav li';
$(selector).on('click', function(){
   $(selector).removeClass('active');
   $(this).addClass('active');
});
 </script>