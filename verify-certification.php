<?php
ob_start();
include("header.php");
include("connection.php");

if (isset ($_POST["submit"])) {
$query = "SELECT * FROM `certification-details`";
$result = mysqli_query($conn,$query);
while($row = mysqli_fetch_array($result)) {
    $reference_number_db = $row['reference_number'];
    $date_of_birth_db = $row['date_of_birth'];
    echo $_POST["date_of_birth"];
    if (($_POST["reference_number"] == $reference_number_db) && ($_POST["date_of_birth"] == $date_of_birth_db)){
        header('location:certificate-detail.php');
        unset($flag);
    } else {
        $flag=1;
    }
}
}
?>

    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-8 ftco-animate text-center text-center mt-5">
          	<p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Receipt Number</span></p>
            <h1 class="mb-3 bread">Receipt Number</h1>
          </div>
        </div>
      </div>
    </div>
 <!-- Services -->
 <div class="container">
    <div class="row">
      <div class="col-12 col-sm-10 col-md-10 col-lg-9 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <div class="img-set text-center">
            <img src="images/hachiweb.png" alt="">
            </div>
            <!-- <h5 class="card-title text-center mt-3">Download Signed Admission Form</h5> -->
            <form action="" class="form-signin my-5" method="POST">
            <div class="row text-center">
            <div class="col-6">
            <div class="form-label-group ">
                <label for="inputEmail my-2">Enter Reference Number</label>
                <input type="text" class="form-control" name="reference_number" id="reference_number" placeholder="Reference Number" maxlength="15" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" required autofocus>
              </div>
            </div>
            <div class="col-6">
            <div class="form-label-group">
                <label for="inputEmail my-2">Enter Date Of Birth</label>
                <input type="date" class="form-control" name="date_of_birth" required>
              </div>
            </div>
            </div>
              <div class="row">
             <div class="col-6">
             <div class="g-recaptcha mt-3" id="rcaptcha" data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
              <span id="captcha" style="color:red"></span>
             </div>
             <div class="col-3 mt-3">
           <?php 
            if(isset($flag) && $flag==1){
               ?> <div class="alert alert-danger">mismached</div>
            <?php }
            ?>
           </div>
              </div>
              <input type="submit" name="submit" class="btn btn-lg btn-primary btn-block text-uppercase my-5" value="SUBMIT"/>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- end Services -->
  <script>
  $(document).on('submit','.form-signin',function(e){
    var v = grecaptcha.getResponse();
    if(v.length == 0)
    {
        document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
        e.preventDefault();
        return false;
    }
    else
    {
        document.getElementById('captcha').innerHTML="Captcha completed";
        return true; 
    }
  });
  </script>
<?php
include("footer.php");
?>