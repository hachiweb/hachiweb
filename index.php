<?php
include("header.php");
?>

<div class="hero-wrap js-fullheight img" style="background-image: url(images/bg_1.jpg);">
    <div class="overlay"></div>
    <div class="container-fluid px-0">
        <div class="row d-md-flex no-gutters slider-text align-items-center js-fullheight justify-content-center">
            <div class="col-md-8 text-center d-flex align-items-center ftco-animate js-fullheight">
                <div class="text mt-5">
                    <h3 class="text-white">Web Development</h3>
                    <h1 class="mb-3 font-weight-bold">We design things with love and passion.</h1>
                    <p>Get best design for your website. Don't lose more clients</p>
                    <p>
                        <a href="portfolio.php" class="btn btn-warning px-5 py-3">Our Work</a>
                        <a href="service.php" class="btn btn-success px-5 py-3 m-4">Services</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section ftco-partner">
    <div class="container">
        <div class="row">
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><img src="images/partner-1.png" class="img-fluid"
                        alt="hachiweb web development company in dehradun"></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><img src="images/partner-2.png" class="img-fluid"
                        alt="hachiweb android and IOS application development company in dehradun"></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><img src="images/partner-3.png" class="img-fluid"
                        alt="hachiweb web design company in dehradun"></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><img src="images/partner-4.png" class="img-fluid"
                        alt="hachiweb php development company in dehradun"></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><img src="images/partner-5.png" class="img-fluid"
                        alt="hachiweb wordpress development company in dehradun"></a>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section services-section bg-light ftco-no-pb">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 text-justify heading-section ftco-animate">
                <h2 class="mb-4 text-center">Why You Should Choose Us</h2>
                <p>At Hachiweb, we develop our trade by maximizing your commerce. Each web plan company points to form
                    cash so do we. But our primary center is to assist your trade to create more cash. Our objective is
                    to construct believe and long term relationship with our clients. Numerous web plan company in
                    Dehradun, Uttrakhand, India fair need a speedy offer without considering client's Return on
                    Speculation (ROI).
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-cloud"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Super Fast Service</h3>
                        <p class="mb-0">Hachiweb is one of the leading Web Advancement Company which offers reliable Web
                            Improvement Administrations around the world to create the foremost momentous comes about &
                            fortify your commerce.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-server"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Daily Backups</h3>
                        <p class="mb-0">What would happen in the event that you woke up tomorrow and your web have had
                            incidentally erased your site? It happened to me. And, you speculated it: I didn't have a
                            reinforcement. That won't ever happen to me once more and I'm here to assist you get
                            arranged as well.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-customer-service"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Technical Services</h3>
                        <p>We Plan & Create, Appealing, Transformation Neighborly, Full Versatile Responsive, Quick
                            Stacking, Security-Optimized and SEO Inviting Websites that will offer assistance to make a
                            important to begin with impression.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-life-insurance"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Secure and Reliable</h3>
                        <p>Site Plan and Advancement experts plan websites with security in Intellect perfect way". The
                            most perfect way to create a location secure and dependable is the application of a firewall
                            to secure your location. The firewall anticipates interruption by blocking the passage of
                            pernicious websites.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-cloud-computing"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">DNS Control</h3>
                        <p>Domain names are shaped by the rules and strategies of the Domain Name System (DNS). Domain
                            enrollment is the method by which a company or individual can secure website space, Once
                            this handle is total the Domain enrollment the space gets to be yours for the period of the
                            contract which is ordinarily one year. And afterward needs to be recharged.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-5 bg-white">
    <section class="ftco-section services-section ftco-no-pb">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 text-center heading-section ftco-animate text-justify">
                    <h2 class="mb-4">Our Focus.</h2>
                    <p>We comprehend your prerequisite and give quality works.Continued accomplishment at Hachiweb has
                        incited us to grow a global group of the most gifted personalities in Web development. Meet the
                        company organization's chiefs and the dedicated individuals who convey creative plans to
                        organizations like yours.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-12 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex align-items-start">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="">
                                <img src="images/neve-envelope-1.png" alt="best web design company dehradun">
                            </span>
                        </div>
                        <div class="media-body pl-4">
                            <h3 class="heading">Web Design</h3>
                            <p class="mb-0">We trust in improving the world with great UI Design. We adored about web
                                improvent related works design & development. Proffesional and SEO easy to use and
                                powerful coding serivices.Our methods are intended to give an organized procedure that
                                is basic, strong, useful and adaptable.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-12 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex align-items-start">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="">
                                <img src="images/neve-map-1.png" alt="best ux/ui design company in uttrakhand">
                            </span>
                        </div>
                        <div class="media-body pl-4">
                            <h3 class="heading">UX/UI Design</h3>
                            <p class="mb-0">Hachiweb is an undeniable UI/UX configuration organization conveying
                                outwardly convincing structures to the customers the whole way across the world.With a
                                group of alluring proficient designers, we pursue structure rules that enable us to
                                configuration exceedingly connected with UI design.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-12 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex align-items-start">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="">
                                <img src="images/neve-phone-1.png" alt="best company for app devlopment in uttrakhand">
                            </span>
                        </div>
                        <div class="media-body pl-4">
                            <h3 class="heading">App Development</h3>
                            <p>Hachiweb Provides you a staggering Android applications for both cell phones and tablet
                                gadgets. Hachiweb has broad involvement in custom portable tablet application
                                advancement. We make immaculate Android applications that upgrade your business and
                                teach in the world wide.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-12 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex align-items-start">
                        <div class="icon ">
                            <span class="">
                                <img src="images/development.png" alt="best website development company in USA" style="width:70px;">
                            </span>
                        </div>
                        <div class="media-body pl-4">
                            <h3 class="heading">Web Development</h3>
                            <p>Hachiweb give Efficient custom programming, which is essential to the accomplishment of
                                any business structure. We emphatically accept that the best web application for your
                                business. Having a quick, well-coded, available website makes an appreciate online
                                encounter and improves well .</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<section class="bg-light" style="padding:30px 0px;">
<div class="container txt-set my-5">
<h1 class="text-center">OUR TEAM</h1>
        <h5 class="text-center">We understand your requirement and provide quality works.</h5>
    <div class="row">
        <div class="col-md-12">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <!-- <li data-target="#myCarousel" data-slide-to="2"></li> -->
                </ol>
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <div class="item carousel-item active">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <span class="">
                                                    <img src="images/rsz_1img_20200216_033650.png" alt="the best Project Manager and Business Development Executive in hachiweb company dehradun" class="w-75">
                                                </span>
                                            </div>
                                            <div class="mt-2 media-body media-body-2">
                                                <h3 class="heading">Prateek Asthana</h3>
                                                <p>Managing Partner</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <span class="">
                                                    <img src="images/aman_img.png" alt="the best Project Manager and Business Development Executive in hachiweb company dehradun" class="w-75">
                                                </span>
                                            </div>
                                            <div class="mt-3 media-body media-body-2">
                                                <h3 class="heading">Aman Batra</h3>
                                                <p>Project Manager and Business Development Executive</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <span class="">
                                                    <img src="images/shaukeen_img.jpg" alt="the best Web Designer & Developer in hachiweb company dehradun" class=""
                                                        style="width:65%;">
                                                </span>
                                            </div>
                                            <div class="mt-3 media-body media-body-2">
                                                <h3 class="heading">Shaukeen Rana</h3>
                                                <p>Web Designer & Developer</p><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item carousel-item">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <span class="">
                                                    <img src="images/TJ7P7PJCU-ULENEBZPT-dc0d87d6a084-512.jpeg" alt="Operation manager @ Hachiweb" class="w-75">
                                                </span>
                                            </div>
                                            <div class="mt-3 media-body media-body-2">
                                            <h3 class="heading">Manbar Singh</h3>
                                                <p>Operation Manager</p><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <span class="">
                                                    <img src="images/IMG-20191106-WA0002.jpg" alt="the best Sr.(Android + iOS) Developer in hachiweb company dehradun" class="w-75">
                                                </span>
                                            </div>
                                            <div class="mt-3 media-body media-body-2">
                                                <h3 class="heading">Mohan Sai Manthri</h3>
                                                <p>Sr.(Android + iOS) Developer</p><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                     
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <span class="">
                                                    <img src="images/rsz_dsc_8465.png" alt="the backend developer at Hachiweb" class="w-75">
                                                </span>
                                            </div>
                                            <div class="mt-3 media-body media-body-2">
                                            <h3 class="heading">Rajendra Kumar</h3>
                                                <p>Backend Developer</p><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item carousel-item">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                            <span class="">
                                                    <img src="images/Sai_Krishna.jpeg" alt="MEAN stack developer at IT company dehradun" class="w-75">
                                                </span>
                                            </div>
                                            <div class="mt-1 media-body media-body-2">
                                                <h3 class="heading">Sai Krishna</h3>
                                                <p>MEAN Stack Developer</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <span class="">
                                                    <img src="images/salesh.jpg" alt="the best Sr. Frontend Developer in Hachiweb company dehradun" class="w-75">
                                                </span>
                                            </div>
                                            <div class="mt-3 media-body media-body-2">
                                                <h3 class="heading">Salesh Kumar</h3>
                                                <p>Sr. Frontend Developer</p><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="shadow bg-light rounded">
                                    <div class="ftco-animate">
                                        <div class="media block-6 services text-center">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <span class="">
                                                    <img src="images/gurpreet.jpg" alt="the best Sr. Backend Developer (Node.js) in hachiweb company dehradun" class="w-75">
                                                </span>
                                            </div>
                                            <div class="mt-3 media-body media-body-2">
                                            <h3 class="heading">Gurpreet Singh</h3>
                                                <p>Sr. Backend Developer (Node.js)</p><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Carousel controls -->
                <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
</section>

<!-- <section class="bg-light" style="padding:50px 0px;">
    <div class="container txt-set my-5">
        <h1 class="text-center">OUR TEAM</h1>
        <h5 class="text-center">We understand your requirement and provide quality works.</h5>
        <div class="row d-flex">
            <div class="col-lg-12 py-5">
                <div class="row pt-md-5">
                    <div class="col-md-3 ftco-animate">
                        <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="">
                                    <img src="images/prateek_sir.jpg" alt="image not found" class="w-75">
                                </span>
                            </div>
                            <div class="mt-3 media-body media-body-2">
                                <h3 class="heading">Prateek Asthana</h3>
                                <p>Founder and CEO</p><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ftco-animate">
                        <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="">
                                    <img src="images/aman_img.png" alt="image not found" class="w-75">
                                </span>
                            </div>
                            <div class="mt-3 media-body media-body-2">
                                <h3 class="heading">Aman Batra</h3>
                                <p>Project Manager and Business Development Executive</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ftco-animate">
                        <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="">
                                    <img src="images/shaukeen_img.jpg" alt="image not found" class="" style="width:65%;">
                                </span>
                            </div>
                            <div class="mt-3 media-body media-body-2">
                                <h3 class="heading">Shaukeen Rao</h3>
                                <p>Web Designer & Developer</p><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ftco-animate">
                        <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="">
                                    <img src="images/IMG-20191106-WA0002.jpg" alt="image not found" class="w-75">
                                </span>
                            </div>
                            <div class="mt-3 media-body media-body-2">
                                <h3 class="heading">Mohan Sai Manthri</h3>
                                <p>Sr.(Android + iOS) Developer</p><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ftco-animate">
                        <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="">
                                    <img src="images/gurpreet.jpg" alt="image not found" class="w-75">
                                </span>
                            </div>
                            <div class="mt-3 media-body media-body-2">
                                <h3 class="heading">Gurpreet Singh</h3>
                                <p>Sr. Backend Developer (Node.js)</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ftco-animate">
                        <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="">
                                    <img src="images/salesh.jpg" alt="image not found" class="w-75">
                                </span>
                            </div>
                            <div class="mt-3 media-body media-body-2">
                                <h3 class="heading">Salesh Kumar</h3>
                                <p>Sr. Frontend Developer</p><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->

<section class="ftco-section ftco-counter img" id="section-counter">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-10 text-center heading-section ftco-animate">
                <h2 class="mb-4">More Than 12,000 Websites Trusted</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                    <div class="text">
                        <strong class="number" data-number="12000">0</strong>
                        <span>CMS Installation</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                    <div class="text">
                        <strong class="number" data-number="100">0</strong>
                        <span>Awards Won</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                    <div class="text">
                        <strong class="number" data-number="10000">0</strong>
                        <span>Registered</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                    <div class="text">
                        <strong class="number" data-number="9000">0</strong>
                        <span>Satisfied Customers</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section bg-primary">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                <h2 class="mb-4">Our Best Pricing</h2>
                <p>We understand your requirement and provide quality works.</p>
            </div>
        </div>
        <div class="row no-gutters d-flex">
            <div class="col-lg-4 col-md-6 ftco-animate d-flex">
                <div class="block-7 bg-light d-flex align-self-stretch">
                    <div class="text-center">
                        <h2 class="heading">Website</h2>
                        <span class="price"><small class="per">Starting from</small>
                            <p class="number">$250</p>
                            <span class="excerpt d-block">100% Forever</span>
                            <h3 class="heading-2 mb-3">Enjoy All The Features</h3>

                            <ul class="pricing-text mb-4">
                                <li><strong>Responsiveness & Quality</strong></li>
                                <li><strong>7-10 Screen</strong></li>
                                <li><strong>Mobile Compatible</strong></li>
                                <li><strong>Platform:-Word Press, Zoho, Wix & Much More</strong></li>
                            </ul>
                            <a href="payment-form.php" class="btn btn-success d-block px-3 py-3 mb-4">Choose Plan</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 ftco-animate d-flex">
                <div class="block-7 d-flex align-self-stretch">
                    <div class="text-center">
                        <h2 class="heading">Android Application</h2>
                        <span class="price"><small class="per">Starting from</small>
                            <p class="number">$250</p>
                            <span class="excerpt d-block">All features are included</span>
                            <h3 class="heading-2 mb-3">Enjoy All The Features</h3>

                            <ul class="pricing-text mb-">
                                <li><strong>Responsiveness & Quality</strong></li>
                                <li><strong>8-10 Screen</strong></li>
                                <li><strong>Included Source Code</strong></li>
                                <li><strong>Admin System & Much More</strong></li>
                            </ul>
                            <a href="payment-form.php" class="btn btn-success d-block px-3 py-3 mt-5">Choose Plan</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 ftco-animate d-flex">
                <div class="block-7 bg-light d-flex align-self-stretch">
                    <div class="text-center">
                        <h2 class="heading">Logo Design</h2>
                        <span class="price"><small class="per">Starting from</small>
                            <p class="number">$50</p>
                            <span class="excerpt d-block">All features are included</span>
                            <h3 class="heading-2 mb-3">Enjoy All The Features</h3>

                            <ul class="pricing-text mb-">
                                <li><strong>Unlimited</strong></li>
                                <li><strong>More Than 10,000+ Design To Choose</strong></li>
                                <li><strong>Custom Design & Much More</strong></li>
                                <li><strong></strong></li>
                            </ul>
                            <a href="payment-form.php" class="btn btn-success d-block px-3 py-3" style="margin-top:80px;">Choose
                                Plan</a>
                    </div>
                </div>
            </div>
            <!-- <div class="col-lg-3 col-md-6 ftco-animate d-flex">
	          <div class="block-7 active d-flex align-self-stretch">
	            <div class="text-center">
		            <h2 class="heading">Pro</h2>
		            <span class="price"><sup>$</sup> <span class="number">99<small class="per">/mo</small></span></span>
		            <span class="excerpt d-block">All features are included</span>
		            <h3 class="heading-2 mb-3">Enjoy All The Features</h3>
		            
		            <ul class="pricing-text mb-4">
		              <li><strong>450 GB</strong> Bandwidth</li>
		              <li><strong>400 GB</strong> Storage</li>
		              <li><strong>$20.00 / GB</strong> Overages</li>
		              <li>All features</li>
		            </ul>
			          <a href="#" class="btn btn-secondary d-block px-3 py-3 mb-4">Choose Plan</a>
	            </div>
	          </div>
	        </div> -->
        </div>
    </div>
</section>

<section class="ftco-section testimony-section testimonial-bg-clr">
    <div class="container">
        <div class="row justify-content-center mb-2">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2 class="mb-4">Our satisfied customers say</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live
                    the blind texts. Separated they live in</p>
            </div>
        </div>
        <div class="row ftco-animate">
            <div class="col-md-12">
                <div class="site-wrap">
                    <div class="site-section">
                        <div class="container">
                            <div class="slide-one-item home-slider owl-carousel">
                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/ReginalGarland.jpg" alt="Owner of “Garland Pro Solutions”, USA" class="img-fluid mb-3">
                                            <h5>Reginald Garland</h5>
                                            <p>Owner of “Garland Pro Solutions”, USA</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;Hachiweb is amazing. They are timely and have great communication.
                                                They seem to almost work 24 hours around the clock and deliver exactly
                                                what they say they will do with a great attitude. They are
                                                fantastic&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>
                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/Rouzier Charles.jpg" alt="Owner of “BellShipItNow”, USA" class="img-fluid mb-3">
                                            <h5>Rouzier Charles</h5>
                                            <p>Owner of “BellShipItNow”, USA</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;After the non-responsiveness behavior of our previous IT team, we
                                                were in desperate need of the web services and that’s when I got
                                                acquainted with Hachiweb. They are super awesome and managed all
                                                technical challenges that had been faced by our previous IT team. I am
                                                more than happy to get their services, permanently for my shipping
                                                business Bell Ship It Now.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/Shelby Lee.jpg" alt="Owner of “The Human Company”, Australia" class="img-fluid mb-3 h-50">
                                            <h5>Shelby Lee</h5>
                                            <p>Owner of “The Human Company”, Australia</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;We, at THE HUMAN COMPANY, help our clients to reach for higher
                                                levels of performance and potential in all areas of their lives through
                                                certified professional coaching. Well, after getting our website
                                                developed by Hachiweb, I must confess that Hachiweb, in the same way,
                                                helped us to reach the high levels online by building an awesome
                                                website, worth of every penny spent.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/Dimiti (1).jpg" alt="Owner of “TruVim LLC”, USA" class="img-fluid mb-3">
                                            <h5>Dimitri Yimga</h5>
                                            <p>Owner of “TruVim LLC”, USA</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;Great services, Hachiweb went above and beyond to make sure all is
                                                well done! They are very committed and customer centric developers and
                                                they makes sure that delivery is as per commitment. I would surely like
                                                to work with them for my future projects.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/Michele Allen.jpg" alt="Owner of “Crux Creative Solutions Private Limited”, USA" class="img-fluid mb-3">
                                            <h5>Michelle Allen</h5>
                                            <p>Owner of “Crux Creative”, USA</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;I tried to find someone for my backend job for my website and it
                                                took several months until I found Hachiweb. They did an amazing job and
                                                were very patient. Highly recommend.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/IMG_9103.jpg" alt="Founder of 'The Sauce ' New Zealand" class="img-fluid mb-3">
                                            <h5>Meg Eriksen</h5>
                                            <p>Founder of 'The Sauce ' New Zealand</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;Developing 'Bad Egg Hire' calculator on our website was not an
                                                easy project and there was a lot of rectifications and effort required
                                                to achieve what I was after but Hachiweb did it. I am very happy with
                                                the final product. Thank you. Good communication.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/Sanja-Neskovic.jpg" alt="Director & Trainer at My Safety First, Canada" class="img-fluid mb-3">
                                            <h5>Sanja</h5>
                                            <p>Director & Trainer at My Safety First, Canada</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;Great communication with Hachiweb. I would like to hire in the future.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/Nima-Khaki.jpg" alt="TeamDigiVision, Denmark" class="img-fluid mb-3">
                                            <h5>Nima Khaki</h5>
                                            <p>TeamDigiVision, Denmark</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;Top notch services and it was pretty simple to get things done from Hachiweb.In the portfolio, just add following websites and about their description.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/659a2748-5449-41c7-b08b-f99b5a057413.webp" alt="Hachiweb reviews" class="img-fluid mb-3">
                                            <h5>Alexy</h5>
                                            <p>FlowerProposals, USA</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;Hachiweb knows what they are doing. very responsive. I will work with them again. Very happy so far.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/mano.jpg" alt="Man Solutions, USA" class="img-fluid mb-3">
                                            <h5>Mano</h5>
                                            <p>Man Solutions, USA</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;Thank you guys for working on this Project.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                                <div>
                                    <div class="testimonial">
                                        <figure class="mb-4">
                                            <img src="images/dara.jpg" alt="Onelifefitness, Ireland" class="img-fluid mb-3">
                                            <h5>Dara</h5>
                                            <p>One L1fe Fitness, Ireland</p>
                                        </figure>
                                        <blockquote>
                                            <p>&ldquo;Hachiweb did a very good job. Very pleased. They are hard workers and were on time. No complaints.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800" rel="stylesheet"> -->
<script>
$(document).ready(function() {
    $(".wish-icon i").click(function() {
        $(this).toggleClass("fa-heart fa-heart-o");
    });
});
</script>
<?php
    include("footer.php");
    ?>