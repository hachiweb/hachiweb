<?php
include("header.php");
?>
    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-8 ftco-animate text-center text-center mt-5">
          	<p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About</span></p>
            <h1 class="mb-3 bread">About</h1>
          </div>
        </div>
      </div>
    </div>
    <section class="container my-5 py-5">
    <div class="row">
        <div class="col-sm-6">
            <img src="images/about.jpg" alt="Hachiweb (HW) is a boutique computerized change consultancy headquartered in Dehradun,Uttrakhand. Since 2015 we have been helping worldwide organizations and built up brands rethink their business by structure effective advanced designing arrangements fueled by the most recent advances technologies." class="border-set">
        </div>
        <div class="col-sm-6 border-set-1">
            <h1>ABOUT US</h1>
            <p>Hachiweb (HW) is a boutique computerized change consultancy headquartered in Dehradun,Uttrakhand. Since 2015 we have been helping worldwide organizations and built up brands rethink their business by structure effective advanced designing arrangements fueled by the most recent advances technologies.</p>
        </div>
    </div>
</section>
    <section class="ftco-section services-section bg-light ftco-no-pb pb-5">
      <div class="container">
      	<div class="row justify-content-center mb-5 pb-5">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">Why You Should Choose Us</h2>
            <p>At Hachiweb, we develop our trade by maximizing your commerce. Each web plan company points to form
              cash so do we. But our primary center is to assist your trade to create more cash. Our objective is
              to construct believe and long term relationship with our clients. Numerous web plan company in
              Dehradun, Uttrakhand, India fair need a speedy offer without considering client's Return on
              Speculation (ROI).</p>
          </div>
        </div>
        <div class="row">
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-cloud"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Super Fast Service</h3>
                        <p class="mb-0">Hachiweb is one of the leading Web Advancement Company which offers reliable Web
                            Improvement Administrations around the world to create the foremost momentous comes about &
                            fortify your commerce.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-server"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Daily Backups</h3>
                        <p class="mb-0">What would happen in the event that you woke up tomorrow and your web have had
                            incidentally erased your site? It happened to me. And, you speculated it: I didn't have a
                            reinforcement. That won't ever happen to me once more and I'm here to assist you get
                            arranged as well.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-customer-service"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Technical Services</h3>
                        <p>We Plan & Create, Appealing, Transformation Neighborly, Full Versatile Responsive, Quick
                            Stacking, Security-Optimized and SEO Inviting Websites that will offer assistance to make a
                            important to begin with impression.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-life-insurance"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">Secure and Reliable</h3>
                        <p>Site Plan and Advancement experts plan websites with security in Intellect perfect way". The
                            most perfect way to create a location secure and dependable is the application of a firewall
                            to secure your location. The firewall anticipates interruption by blocking the passage of
                            pernicious websites.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-flex align-items-start">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="flaticon-cloud-computing"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">DNS Control</h3>
                        <p>Domain names are shaped by the rules and strategies of the Domain Name System (DNS). Domain
                            enrollment is the method by which a company or individual can secure website space, Once
                            this handle is total the Domain enrollment the space gets to be yours for the period of the
                            contract which is ordinarily one year. And afterward needs to be recharged.</p>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-counter img" id="section-counter">
    	<div class="container">
    		<div class="row justify-content-center mb-5">
          <div class="col-md-10 text-center heading-section ftco-animate">
            <h2 class="mb-4">More Than 12,000 Websites Trusted</h2>
          </div>
        </div>
    		<div class="row justify-content-center">
          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="12000">0</strong>
                <span>CMS Installation</span>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="100">0</strong>
                <span>Awards Won</span>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="10000">0</strong>
                <span>Registered</span>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="9000">0</strong>
                <span>Satisfied Customers</span>
              </div>
            </div>
          </div>
        </div>
    	</div>
    </section>

    <!-- <section class="ftco-section ftco-no-pt bg-light ftco-no-pb">
    	<div class="container">
    		<div class="row d-flex">
    			<div class="col-lg-6 order-lg-last d-flex">
    				<div class="bg-primary py-md-5 d-flex align-self-stretch">
    					<div class="main">
		    				<img src="images/undraw_data_report_bi6l.svg" class="img-fluid svg" alt="">
		    				<div class="heading-section heading-section-white ftco-animate mt-5 px-3 px-md-5">
			            <h2 class="mb-4">Our Main Services</h2>
			            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
			          </div>
		          </div>
	          </div>
    			</div>
    			<div class="col-lg-6 py-5">
    				<div class="row pt-md-5">
    					<div class="col-md-6 ftco-animate">
    						<div class="media block-6 services text-center">
		            	<div class="icon d-flex align-items-center justify-content-center">
		            		<span class="flaticon-cloud-computing"></span>
		            	</div>
		              <div class="mt-3 media-body media-body-2">
		                <h3 class="heading">Cloud VPS</h3>
		                <p>Even the all-powerful Pointing has no control about the blind texts</p>
		              </div>
		            </div>
    					</div>
    					<div class="col-md-6 ftco-animate">
    						<div class="media block-6 services text-center">
		            	<div class="icon d-flex align-items-center justify-content-center">
		            		<span class="flaticon-cloud"></span>
		            	</div>
		              <div class="mt-3 media-body media-body-2">
		                <h3 class="heading">Share</h3>
		                <p>Even the all-powerful Pointing has no control about the blind texts</p>
		              </div>
		            </div>
    					</div>
    					<div class="col-md-6 ftco-animate">
    						<div class="media block-6 services text-center">
		            	<div class="icon d-flex align-items-center justify-content-center">
		            		<span class="flaticon-server"></span>
		            	</div>
		              <div class="mt-3 media-body media-body-2">
		                <h3 class="heading">VPS</h3>
		                <p>Even the all-powerful Pointing has no control about the blind texts</p>
		              </div>
		            </div>
    					</div>
    					<div class="col-md-6 ftco-animate">
    						<div class="media block-6 services text-center">
		            	<div class="icon d-flex align-items-center justify-content-center">
		            		<span class="flaticon-database"></span>
		            	</div>
		              <div class="mt-3 media-body media-body-2">
		                <h3 class="heading">Dedicated</h3>
		                <p>Even the all-powerful Pointing has no control about the blind texts</p>
		              </div>
		            </div>
    					</div>
    				</div>
          </div>
    		</div>
    	</div>
    </section> -->
    <?php
    include("footer.php");
    ?>