<?php
include("header.php");
?>
<link rel="stylesheet" href="build/css/intlTelInput.css">
<link rel="stylesheet" href="build/css/demo.css">
<div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');"
    data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-8 ftco-animate text-center text-center mt-5">
                <p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.php">Home <i
                                class="ion-ios-arrow-forward"></i></a></span> <span>Payment</span></p>
                <h1 class="mb-3 bread">Payment</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section contact-section bg-primary">
    <div class="container">
        <div class="row block-9 justify-content-center mb-5">
            <div class="col-md-12 mb-md-5 questions">
                <h2 class="text-center text-uppercase">If you got any questions please <a href="contact.php" class="btn btn-warning text-white shadow border-0">Click Here</a> to send us a
                        message</h2>
                <form action="#" class="bg-light p-5 contact-form" method="POST">
                    <div class="row contact_sec">
                        <div class="col-sm-12 mb-5">
                            <div class="contact_detail text-center">
                                <span class="text-center shadow">Contact Details</span>
                            </div>
                        </div><!-- col-12-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name" name="contact_first_name"
                                    autocomplete="on" required autofocus>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name" name="contact_last_name" required>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address Line1" name="contact_address_line1"
                                    required>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address Line2" name="contact_address_line2"
                                    required>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control d-block" placeholder="Zip Code" name="contact_zip_code"
                                    minlength="7" maxlength="7"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    required>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="tel" class="form-control phone_number" id="phone_number1" placeholder=""
                                    name="contact_phone_number" required>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Enter Email Id" name="contact_email_id"
                                    required>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select id="country1" name="contact_country_name" class="form-control country" required>
                                    <option value="" selected>Select Country </option>
                                </select>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select name="contact_state_name" id="state1" class="form-control" required>
                                    <option value="" selected>Select State/Province </option>
                                </select>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter City" name="contact_city_name" required>
                            </div>
                        </div><!-- col-6-->
                    </div> <!-- end contect details -->

                    <div class="row">
                        <div class="col-12 my-5">
                            <div class="contact_detail text-center">
                                <span class="text-center shadow">Project Description / Requirement</span>
                            </div>
                        </div><!-- col-12-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="project_services_name" id="" class="form-control" required>
                                    <option value="" selected> Select Service </option>
                                    <option value="website">Website Development</option>
                                    <option value="application">Application Development</option>
                                    <option value="logo_design">Logo Design</option>
                                </select>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="project_file_upload_name">
                                    <label class="custom-file-label" for="customFile">Supported File / Reference
                                        Upload</label>
                                </div>
                            </div>
                        </div><!-- col-6-->
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control" rows="4" id="comment" name="project_description"
                                    placeholder="Description"></textarea>
                            </div>
                        </div> <!-- col-12-->
                    </div> <!-- end project description -->

                    <div class="row">
                        <!-- start billing details -->
                        <div class="col-12 my-5">
                            <div class="contact_detail text-center">
                                <span class="text-center shadow">Billing Details</span>
                            </div>
                        </div><!-- col-12-->
                        <div class="col-md-12 text-center">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="check-billing"
                                        name="example1" checked onchange="valueChanged()">
                                    <label class="custom-control-label" for="check-billing">Uncheck this if your Billing
                                        Details are not same as Contact Details. If same ignore this option and goto Payment Details.</label>
                                </div>
                            </div>
                            <div class="row mt-3" style="display:none;" id="billing-detail">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First Name"
                                            name="bill_first_name" autocomplete="on" required>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Last Name" name="bill_last_name"
                                            required>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address Line1"
                                            name="bill_address_line1" required>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address Line2"
                                            name="bill_address_line2" required>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control d-block" placeholder="Zip Code"
                                            name="bill_zip_code" minlength="7" maxlength="7"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            required>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="tel" class="form-control phone_number" id="phone_number2"
                                            placeholder="" name="bill_phone_number" required>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Enter Email Id"
                                            name="bill_email_id" required>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select id="country2" name="bill_country_name" class="form-control country" required>
                                            <option selected>Select Country </option>
                                        </select>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select name="bill_state_name" id="state2" class="form-control" required>
                                            <option value="" selected>Select State/Province </option>
                                        </select>
                                    </div>
                                </div><!-- col-6-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter City" name="bill_city_name"
                                            required>
                                    </div>
                                </div><!-- col-6-->
                            </div>
                        </div><!-- col-12-->
                        <div class="col-md-12">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                    <!--end billing Details -->

                    <div class="row">
                        <!--start Payment Details -->
                        <div class="col-12 my-5">
                            <div class="contact_detail text-center">
                                <span class="text-center shadow">Payment Details</span>
                            </div>
                        </div><!-- col-12-->
                        <div class="col-md-12">
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <div>
                                        <h5>Billing Information</h5>
                                        <h6>Enter Your Payment Details Bellow</h6>
                                    </div>
                                </div><!-- col-12-->
                                </div><!-- row-->
                                <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12 my-auto">
                                    <div class="payment-method">
                                        <h5>Payment Method <span class="text-danger">*</span></h5>
                                    </div>
                                </div><!-- col-4-->
                                <div class="col-lg-5 col-md-8 col-sm-12">
                                    <!-- <div class="payment_method_other">
                                        <img src="images/visa.png" alt="image not found">
                                    </div> -->
                                    <div class="payment_method_other cc-selector-2">
                                        <input id="paypal" type="radio" class="payment_gateway form-control" name="paypal" value="paypal" />
                                        <label class="drinkcard-cc paypal" for="paypal"></label>
                                        <!-- <input id="visa" type="radio" class="payment_gateway" name="paypal" value="visa" />
                                        <label class="drinkcard-cc visa"for="visa"></label> -->
                                    </div>
                                    <div class="payment_method_india cc-selector-2">
                                        <input id="visa2" type="radio" class="payment_gateway form-control" name="for_india" value="payu" />
                                        <label class="drinkcard-cc payu" for="visa2"></label>
                                        <input id="ccavenue" type="radio" class="payment_gateway" name="for_india" value="ccavenue" />
                                        <label class="drinkcard-cc ccavenue" for="ccavenue"></label>
                                    </div>
                                </div><!-- col-5-->
                                <div class="col-lg-4 col-md-12 col-sm-12 my-auto">
                                    <div class="payble_amount">
                                        <h5 class="shadow">Payable Amount: <span>$250</span></h5>
                                    </div>
                                </div><!-- col-6-->
                            </div>
                        </div><!-- col-12-->
                        <div class="col-md-12">
                        <div class="g-recaptcha mt-3" id="rcaptcha"  data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
                        <span id="captcha" style="color:red"></span>
                        </div>
                    </div>
                    <!--row-->
                    <div class="row submit-button">
                        <div class="col-sm-6 col-6">
                            <button type="submit" class="btn btn-primary btn_submit shadow"> SUBMIT <i class="fas fa-paper-plane"></i></button>
                        </div>
                        <div class="col-sm-6 col-6 right-align">
                        <button type="reset" class="btn btn-primary btn_reset shadow"> RESET <i class="fas fa-redo"></i></button>
                        </div>
                    </div>
                </form>
            </div><!-- col-12-->
        </div>
    </div>
</section>
<script type="text/javascript" src="js/countries.js"></script>
<script src="build/js/intlTelInput.js"></script>
<!-- <script>
$(document).ready(function(){
    $('select').on('change', function() {
    if (this.value == '-1') {
        $('optgroup option').prop('disabled', true);
    } else {
        $('optgroup option').prop('disabled', false);
    }
});
});
</script> -->

<?php
    include("footer.php");
?>