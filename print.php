<?php
if (session_status() == 0) {
}
session_start();

// echo session_id();
// echo ini_get('session.cookie_domain');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// echo "<pre>";
// print_r($_SESSION['contact']['contact_first_name']);
// print_r($_SESSION['contact']['contact_last_name']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style>
    </style>
</head>
<style>
.container{
    margin-bottom:50px;
}
.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
.table-condensed thead tr td{
    border-top: none;
}
</style>
<body>

<div class="container">
    <div class="row">
        <div class="col-12">
    		<div class="d-flex justify-content-between my-3">
                <h2>Invoice</h2>
                <h3 class="pull-right">Order # 12345</h3>
            </div>
            <div class="border-top "></div>
    		<div class="row mt-3">
    			<div class="col-6">
    				<address>
    				<strong>Billed Details:</strong><br>
    					John Smith<br>
    					1234 Main<br>
    					Apt. 4B<br>
    					Springfield, ST 54321
    				</address>
    			</div>
    			<div class="col-6 text-right">
    				<address>
        			<strong>Contact Details:</strong><br>
					<?php echo $_SESSION['contact']['contact_first_name'].' '.$_SESSION['contact']['contact_last_name']; ?><br>
					<?php echo $_SESSION['contact']['contact_address_line1'] ?><br>
					<?php echo $_SESSION['contact']['contact_address_line2']; ?><br>
					<?php echo $_SESSION['contact']['contact_zip_code']; ?>
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-6">
    				<address>
    					<strong>Payment Method:</strong><br>
    					Visa ending **** 4242<br>
                        <?php echo $_SESSION['contact']['contact_email_id']; ?><br>
                        <?php echo $_SESSION['contact']['contact_phone_number']; ?>
                        
    				</address>
    			</div>
    			<div class="col-6 text-right">
    				<address>
    					<strong>Order Date:</strong><br>
    					<?php echo date('d-m-Y'); ?><br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="card card-default">
    			<div class="card-header">
    				<h3 class="card-title">Order summary</h3>
    			</div>
    			<div class="card-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Service</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-center"><strong>Quantity</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<tr>
    								<td><strong><?php echo $_SESSION['contact']['project_services_name']; ?></strong></td>
    								<td class="text-center">$10.99</td>
    								<td class="text-center">1</td>
    								<td class="text-right">$10.99</td>
    							</tr>
    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal</strong></td>
    								<td class="thick-line text-right">$10.99</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>SGST 9%</strong></td>
    								<td class="no-line text-right">$10.66</td>
                                </tr>
                                <tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>CGST 9%</strong></td>
    								<td class="no-line text-right">$10.66</td>
                                </tr>
                                <!-- <tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>IGST</strong></td>
    								<td class="no-line text-right">$10.66</td>
    							</tr> -->
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center thick-line"><strong>Total</strong></td>
    								<td class="no-line text-right thick-line">$685.99</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
</body>

</html>
<?php 

session_destroy();
?>